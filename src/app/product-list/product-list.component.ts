import { Component, OnInit } from '@angular/core';
import products, {ProductInterface} from '../product'
@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.less']
})

export class ProductListComponent implements OnInit {
  public products: ProductInterface[] | undefined

  constructor() {
  }

  ngOnInit(): void {
    console.log(products)
    this.products = products
  }

  share(){
    window.alert('The product has been shared!');
  }
  onNotify(){
    window.alert("You will be notified when the product is cheap")
  }

}
