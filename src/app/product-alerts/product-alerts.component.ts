import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import products, {ProductInterface} from '../product'


@Component({
  selector: 'app-product-alerts',
  templateUrl: './product-alerts.component.html',
  styleUrls: ['./product-alerts.component.less']
})
export class ProductAlertsComponent implements OnInit {
  @Input() product!: ProductInterface;
  @Output() notify = new EventEmitter()
  constructor() { }

  ngOnInit(): void {
  }

}
