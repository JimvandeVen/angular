export interface ProductInterface {
  id: number
  name: string
  description: string
  price: number
}

export default [
    {
      id: 34567,
      name: "schoentje",
      description: "Zit echt heel lekker",
      price: 12345,
    },
    {
      id: 5674899,
      name: "t-Shirt",
      description: "Heel erg zwart",
      price: 11,
    },
    {
      id: 5674899,
      name: "computer",
      description: "Met heel veel video kaart dingen",
      price: 67,
    },
    {
      id: 5674899,
      name: "gieter",
      description: "Met gaatjes",
      price: 63636,
    },

  ]
